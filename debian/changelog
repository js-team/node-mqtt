node-mqtt (4.3.7-5) unstable; urgency=medium

  * patch: longer timeouts

 -- Jérémy Lal <kapouer@melix.org>  Tue, 11 Feb 2025 10:15:57 +0100

node-mqtt (4.3.7-4) unstable; urgency=medium

  * Increase timeout for pkg test

 -- Jérémy Lal <kapouer@melix.org>  Fri, 29 Nov 2024 19:22:29 +0100

node-mqtt (4.3.7-3) unstable; urgency=medium

  * Team upload
  * Update standards version to 4.6.2, no changes needed
  * Add missing dependency to node-lru-cache (Closes: #1056705)

 -- Yadd <yadd@debian.org>  Sat, 25 Nov 2023 07:36:53 +0400

node-mqtt (4.3.7-2) unstable; urgency=medium

  * Team upload
  * Update standards version to 4.6.1, no changes needed.
  * Replace /releases by /tags in GitHub urls

 -- Yadd <yadd@debian.org>  Wed, 02 Nov 2022 08:45:47 +0100

node-mqtt (4.3.7-1) unstable; urgency=low

  * New upstream version 4.3.7
  * debian/control: add node-number-allocator and node-rfdc to Depends.
  * debian/patches
    - add random-server-port.patch: Fix node-js doesn't reuse port.
    - remove-failed-test.patch: Don't run some failed test.

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Sun, 26 Jun 2022 14:34:30 +0800

node-mqtt (4.2.8-1) unstable; urgency=medium

  * Team upload
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.0, no changes needed.
  * Add "Rules-Requires-Root: no"
  * Fix filenamemangle
  * Fix GitHub tags regex
  * Use dh-sequence-nodejs instead of pkg-js-tools
  * Update nodejs dependency to nodejs:any
  * New upstream version 4.2.8
  * Reduce dependencies to those really needed
  * Use pk-js-autopkgtest with a debian/tests/autopkgtest-pkg-nodejs.conf

 -- Yadd <yadd@debian.org>  Wed, 01 Dec 2021 16:43:34 +0100

node-mqtt (4.2.6-2) unstable; urgency=low

  * autopkgtest: some tests take longer time than before thus we increase
    timeout of mocha (Closes: #978988)

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Sat, 02 Jan 2021 03:37:22 +0800

node-mqtt (4.2.6-1) unstable; urgency=low

  * New upstream release.

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Mon, 30 Nov 2020 17:12:34 +0800

node-mqtt (4.2.5-1) unstable; urgency=low

  * New upstream release.

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Sat, 21 Nov 2020 01:37:33 +0800

node-mqtt (4.2.4-1) unstable; urgency=low

  * New upstream release.

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Thu, 12 Nov 2020 08:09:57 +0800

node-mqtt (4.2.1-1) unstable; urgency=low

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).

  [ Ying-Chun Liu (PaulLiu) ]
  * New upstream version 4.2.1
  * Remove debian/patches/0001_add_help_path.patch: already upstreamed

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Thu, 10 Sep 2020 05:49:27 +0800

node-mqtt (4.1.0-1) unstable; urgency=low

  [ Ying-Chun Liu (PaulLiu) <paulliu@debian.org> ]
  * New upstream release.

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Tue, 30 Jun 2020 10:43:23 +0800

node-mqtt (4.0.0-2) unstable; urgency=low

  [ Ying-Chun Liu (PaulLiu) <paulliu@debian.org> ]
  * Add chai to debian/tests/control

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Wed, 20 May 2020 01:18:52 +0800

node-mqtt (4.0.0-1) unstable; urgency=low

  [ Ying-Chun Liu (PaulLiu) <paulliu@debian.org> ]
  * New upstream release.

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Tue, 19 May 2020 12:59:03 +0800

node-mqtt (3.0.0-3) unstable; urgency=low

  [ Ying-Chun Liu (PaulLiu) <paulliu@debian.org> ]
  * Make the build reproducible (Closes: #958382)
    - Thanks to "Chris Lamb" <lamby@debian.org>

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Mon, 27 Apr 2020 02:23:57 +0800

node-mqtt (3.0.0-2) unstable; urgency=low

  * Fix debci fail. Adding openssl as test dependencies.

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Sun, 26 Apr 2020 12:12:56 +0800

node-mqtt (3.0.0-1) unstable; urgency=low

  [ Ying-Chun Liu (PaulLiu) <paulliu@debian.org> ]
  * Initial release (Closes: #816028)

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Wed, 05 Feb 2020 19:24:38 +0800
